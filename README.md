# Vagrantify

Vagrantify is a ready to use configuration that combine the power of [Ansible] and the flexibility of [Vagrant] in order to offer the best and fast and custom developer playground. Vagrantfy project came with:

  - clean and light weight box
  - easy to use configuration file : the famous 'vagrantify.yml' file that command all your box
  - unobtrusive structure to let you keep your project code clean and sperated from the vagrantify box
  - fast, fast, fast !! juste some couple of minutes and you vm is ready ;)

Vagrantify project try to offer fo each developer a complete solution that contain :

  - full control over vm (box, cpu, memory, etc.)
  - full control over network settings (private ip, ports, etc.)
  - extensible system packages installation process : specify needed packages and let vagrantify work ;)
  - full control over nginx configuration with the possibility to configure custom web server
  - PHP-FPM and possibility to select between version 5.4 or 5.5 or 5.6
  - extensible php packages installation process : specify needed php packages and let vagrantify work
  - extensible php extensions installation process : specify needed php extensions and let vagrantify work
  - full control over xdebug configuration 
  - full control over mysql installation:
    - customizable root password
    - customizable port
    - customizable root password
    - choose if you want to allow remote access
    - create as much database as you want within the installation process
    - create as much users as you want within the installation process and assign privileges
  - nodejs and npm installed and ready to use
  - composer installed and ready to use
  - symfony bonus :
    - symfony installer installed and ready to use
    - custom cache and logs (with valid permissions) in custom folders : no more slowness

### Version
  * 1.0 : *26/06/2015*

### Installation

  - head up to the [vagrant] and [ansible] and [virtualbox] websites and just install :P
  - within your git project just run :
```sh
$ git submodule add https://fnayou@bitbucket.org/fnayou/vagrantify.git vagrantify
$ cp vagrantify/vagrantify.yml.dist vagrantify.yml
```
  - open up the vagrantify.yml(fully commented) in your favorite editor, make your magic, and save it
  - now, everything is ready to be installed so, jusr run :
```sh
$ cd vagrantify
$ vagrant up
```
  - wait for a couple of minutes
  - that's it ! your playground is ready ;)

### Vagarnt Plugins

Vagrantify, can be setup with any/all of the following vagrant plugin

* Landrush (https://github.com/phinze/landrush)
* Vagrant-Cachier (https://github.com/fgrehm/vagrant-cachier)
* Vagrant-HostManager (https://github.com/smdahlen/vagrant-hostmanager)

### Development

Want to contribute? Great!

  1. fork it
  2. create your feature branch
  3. commit you changes
  4. push to the branch
  5. submit a pull request :D

### Issues

Vagrantify will have issues like any other project, so please feel free to add all your [issues]. we will try to do our best to fix Vagrantify asap.

### Todo's

  * treat all todos within project
  * add more role like :
    * mailcatcher
    * apache and let user to choose between nginx and apache
    * capifony for deployment
    * jenkins as ci
    * etc.

License
----

MIT


**Free Software, Hell Yeah!**

[ansible]:http://www.ansible.com/
[vagrant]:https://www.vagrantup.com/
[virtualbox]:https://www.virtualbox.org/
[issues]:https://bitbucket.org/fnayou/vagrantify/issues
