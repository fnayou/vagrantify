# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

####################################
# Vagrantfile for Vagrantify Project
#
# WARNING: THIS IS A WORK IN PROGRESS.
#
# DO NOT USE IN PRODUCTION. THIS IS FOR DEVELOPMENT/TESTING PURPOSES ONLY.
#
# ONLY TESTED with VirtualBox provider.
# todo: have to simplify this file
# todo: add some documentation/comments to the vagrantify.yml.dist
# todo: create vagrantify boxes
# todo: add support of multiple web servers
####################################

#==========================================================================
# We will try to load custom configuration from 'vagrantify.yml'
#
# It reads some basic configs from our 'default.yml' and 'vagrantify.yml'
# in order to decide how to start up the Vagrant VM.
#==========================================================================
dir = File.dirname(File.expand_path(__FILE__))
require 'yaml'
require "#{dir}/vagrantify/config/deep_merge.rb"

# Load configuration file
# First we load 'vagrantify/config/default.yml'
# todo : have to manage merging in separate class
vagrantify = YAML.load_file('vagrantify/config/default.yml')

# Next, load custom overrides from 'vagrantify.yml'
# If it doesn't exist, no worries. We'll just use the defaults
if File.exists?("#{dir}/../vagrantify.yml")
  vagrantify.deep_merge!(YAML.load_file("#{dir}/../vagrantify.yml"))
end

# At this point, all our configs can be referenced as vagrantify['key']['key'], e.g. vagrantify['vm']['box']

####################################
# Currently, we require Vagrant 1.6.0 or above.
Vagrant.require_version '>= 1.6.0'

# Vagrant configs
Vagrant.configure('2') do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  #-----------------------------
  # Basic Box Settings
  #-----------------------------
  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box       = vagrantify['vm']['box']

  # The url from where the 'config.vm.box' box will be fetched if it
  # doesn't already exist on the user's system.
  if vagrantify['vm']['box_url']
    config.vm.box_url = vagrantify['vm']['box_url']
  end

  # define this box so Vagrant doesn't call it "default"
  config.vm.define "vagrantify"

  #-----------------------------
  # Sharing Settings
  #-----------------------------
  # we will configure the actual and parent folder, one for the vagrant, the second to be used for application staff
  config.vm.synced_folder '.',  '/vagrant', id: 'vagrant-root', :nfs => true
  config.vm.synced_folder '..', '/var/www', id: 'application',  :nfs => true

  #-----------------------------
  # Network Settings
  #-----------------------------
  # configure a private network and set this guest's IP to 10.0.0.2
  config.vm.network :private_network, ip: vagrantify['network']['ip']

  # Create a forwarded ports mapping which allows access to a specific port
  # within the machine from a port on the host machine. You can define
  # as many ports as you want in the 'vagrantify.yml' file
  vagrantify['network']['ports'].each do |i, port|
    if port['guest'] != '' && port['host'] != ''
      config.vm.network :forwarded_port, guest: port['guest'].to_i, host: port['host'].to_i, auto_correct: true
    end
  end

  # If a port collision occurs (i.e. port 8080 on local machine is in use),
  # then tell Vagrant to use the next available port between 8081 and 8100
  # todo: have to include port range
  # config.vm.usable_port_range = 8081..8100

  #------------------------------
  # DNS Settings (if enabled)
  #------------------------------
  # BEGIN Landrush (https://github.com/phinze/landrush) configuration
  # This section will only be triggered if you have installed "landrush"
  #     vagrant plugin install landrush
  if Vagrant.has_plugin?('landrush')
    config.landrush.enable
    # let's use the Google free DNS
    config.landrush.upstream '8.8.8.8'
    config.landrush.guest_redirect_dns = false
  end
  # END Landrush configuration

  #------------------------------
  # Caching Settings (if enabled)
  #------------------------------
  # BEGIN Vagrant-Cachier (https://github.com/fgrehm/vagrant-cachier) configuration
  # This section will only be triggered if you have installed "vagrant-cachier"
  #     vagrant plugin install vagrant-cachier
  if Vagrant.has_plugin?('vagrant-cachier')
    # Use a vagrant-cachier cache if one is detected
    config.cache.auto_detect = true

    # set vagrant-cachier scope to :box, so other projects that share the
    # vagrant box will be able to used the same cached files
    config.cache.scope = :box
  end
  # END Vagrant-Cachier configuration

  #----------------------------------
  # HostManager Settings (if enabled)
  #----------------------------------
  # BEGIN Vagrant-HostManager (https://github.com/smdahlen/vagrant-hostmanager) configuration
  # This section will only be triggered if you have installed "vagrant-hostmanager"
  #     vagrant plugin install vagrant-hostmanager
  if Vagrant.has_plugin?('vagrant-hostmanager')
    # We will tweak Host hosts file to include ip and hostname
    hosts = Array.new()
    hosts.push(vagrantify['server']['server_name'])
    hosts.push("www.#{vagrantify['server']['server_name']}")

    # Common plugin configuration
    config.hostmanager.enabled           = true
    config.hostmanager.manage_host       = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline   = false
    config.hostmanager.aliases           = hosts
  end
  # END Vagrant-HostManager configuration

  #-----------------------------
  # Basic System Customizations
  #-----------------------------

  # Actually run 'ansible' to setup the server
  config.vm.provision :ansible do |ansible|
    # Dump general configuration to 'ansible' to be used in playbook
    # todo : have to manage file generation in separate class
    vagrantify_vars = File.new('vagrantify/provisions/vagrantify_vars.yml', 'w')
    vagrantify_vars.write(vagrantify.to_yaml)
    vagrantify_vars.close

    ansible.extra_vars = { ansible_ssh_user: 'vagrant' }
    ansible.playbook = 'vagrantify/provisions/site.yml'
  end

  #############################################
  # Customized provider settings for VirtualBox
  # Many of these settings use VirtualBox's
  # 'VBoxManage' tool: http://www.virtualbox.org/manual/ch08.html
  #############################################
  config.vm.provider :virtualbox do |vb|
    # Boot into GUI mode (login: vagrant, pwd: vagrant). Useful for debugging boot issues, etc.
    vb.gui = vagrantify['vm']['gui']

    # Name of the VM created in VirtualBox (Also the name of the subfolder in ~/VirtualBox VMs/ where this VM is kept)
    vb.customize [ 'modifyvm', :id, '--name', vagrantify['vm']['hostname'] ]

    # Use VBoxManage to provide Virtual Machine with extra memory (default is only 512MB)
    vb.customize [ 'modifyvm', :id, '--memory', vagrantify['vm']['memory'] ]

    # define number of cpu used in the VM (defaults is only 1)
    vb.customize [ 'modifyvm', :id, '--cpus', vagrantify['vm']['cpus'] ]

    if vagrantify['vm']['max_cpu']
      # Use VBoxManage to ensure Virtual Machine only has access to a percentage of host CPU
      vb.customize ['modifyvm', :id, '--cpuexecutioncap', vagrantify['vm']['max_cpu']]
    end

    # Use VBoxManage to have the Virtual Machine use the Host's DNS resolver
    vb.customize ['modifyvm', :id, '--natdnshostresolver1', 'on']
  end
end
